
import java.util.concurrent.locks.*;
public class LamportQueue {
    
    private volatile int head;
    private volatile int tail;
    private volatile Packet[] queue;
    public volatile ReentrantLock lock;
    @SuppressWarnings({"unchecked"})
    public LamportQueue(int queueDepth, ReentrantLock lock) {
        this.lock = lock;
        queue = new Packet[queueDepth];
        head = 0;
        tail = 0;
    }

    public boolean isEmpty() {
        return tail == head;
    }

    public void enqueue(Packet packet) throws FullException {
        if (tail - head == queue.length) {
            throw new FullException(packet);
        }
        queue[tail % queue.length] = packet;
        tail = tail + 1;
    } 


    public Packet dequeue() throws EmptyException {
        if (tail == head) {
            //System.out.println("EMPYT @@@@@@@@@@@@@@@@@@@@@@@@@@@");
            throw new EmptyException();
        }
        //System.out.println(t);
        Packet packet = queue[head % queue.length];
        if (packet == null) {
            throw new EmptyException();
        }
        head = head + 1;
        return packet;
    } 
}
