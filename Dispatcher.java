import org.deuce.Atomic;
public class Dispatcher implements Runnable {
    
    public LamportQueue[] configQueues;
    public LamportQueue[] dataQueues;
    public PacketGenerator packetSource;
    public int numQueues;
    public boolean isDone = false;
    public int totalPackets;
    PaddedPrimitiveNonVolatile done;
    public Dispatcher(LamportQueue[] configQueues,
                      LamportQueue[] dataQueues,
                      PacketGenerator source,
                      int numWorkers,
                      PaddedPrimitiveNonVolatile done){
        this.configQueues = configQueues;
        this.dataQueues = dataQueues;
        this.packetSource = source;
        this.numQueues = numWorkers;
        this.totalPackets = 0;
        this.done = done;
    }
    
    public void run() {
        //pointers to queues
        while (!(Boolean)done.value) {
            processPacket();
        }
        //System.out.println("finishing");
        isDone = true;
    }

    //@Atomic
    public void processPacket() {
        int currentConfigQueue = 0;
        int currentDataQueue = 0;
        Packet packet;
        packet = packetSource.getPacket();
        if (packet.type == Packet.MessageType.ConfigPacket) {
            //keep trying to enqueue, probably change this later
            while (true) {
                try {
             //       System.out.println("enqueing config packet to "+currentConfigQueue);

              //      System.out.println("config length "+this.configQueues.length);
                    this.configQueues[currentConfigQueue].enqueue(packet);
                    currentConfigQueue = (currentConfigQueue + 1) % this.configQueues.length;
                    break;
               //     System.out.println("enqueing config packet to "+currentConfigQueue);
//                    System.out.println("next config packet");
                }
                catch (FullException e) { 

                    //System.out.println("full");
                    if ((Boolean) done.value) {
                        break;
                    }
                }
            }
        }
        else if (packet.type == Packet.MessageType.DataPacket) {
            //keep trying to enqueue, probably change this later
            while (true) {
                try {
                    this.dataQueues[currentDataQueue].enqueue(packet);
                    currentDataQueue = (currentDataQueue + 1) % this.dataQueues.length;
                    //System.out.println("next data packet");
                    break;
                }
                catch (FullException e) { 
                    if ((Boolean) done.value) {
                        break;
                    }
                }
            }
        }
        else {
            System.out.println("trouble yo!");
        }

    }

    public boolean isDone() {
        return isDone;
    }


}
