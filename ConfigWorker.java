import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.*;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.Random;
import org.deuce.Atomic;


class ConfigWorker implements PacketWorker {

  PacketGenerator source;
  PaddedPrimitiveNonVolatile<Boolean> done;
  PaddedPrimitive<Boolean> memFence;
  List<AtomicInteger> histogram;
  LockFreeHashTable<Boolean> PNG;
  LockFreeHashTable<ConcurrentSkipListMap<Double,Boolean>> R;
  LockFreeHashTable<Boolean> cache;
  LamportQueue[] configQueues;
  int id;

  Fingerprint fingerprint;
  int totalPackets;

  public ConfigWorker(
               PacketGenerator source,
               PaddedPrimitiveNonVolatile<Boolean> done,
               PaddedPrimitive<Boolean> memFence,
               List<AtomicInteger> histogram,
               LockFreeHashTable<Boolean> PNG,
               LockFreeHashTable<ConcurrentSkipListMap<Double,Boolean>> R,
               LockFreeHashTable<Boolean> cache,
               LamportQueue[] configQueues,
               int id) {

    this.source = source;
    this.done = done;
    this.memFence = memFence;
    this.histogram = histogram;
    this.PNG = PNG;
    this.R = R;
    this.cache = cache;
    this.configQueues = configQueues;
    this.id = id;
    this.fingerprint = new Fingerprint();
    this.totalPackets = 0;
  }

  public void run() {
  //  System.out.println("starting config");
    Random random = new Random();
    int queueIndex = -1;
    while (!done.value) {
      //hand craft this later
      // System.out.println("process config packet "+this.id);
      //queueIndex = random.nextInt(this.configQueues.length);
      processPacket(this.id);
    }  
    //flush out those in my queue
    while (!this.configQueues[this.id].isEmpty()) {
      // System.out.println("never empty "+this.id);
      processPacket(this.id);
      //  System.out.println("finished process"+this.id);
    }

  }
  
  public void processPacket(int queueIndex) {
      try {
        Packet packet = this.configQueues[queueIndex].dequeue();
        processConfigPacket(packet);
        //cache.removeAll();
        totalPackets++;
        // System.out.println(this.id+" Total packets: " + totalPackets + " " + queueIndex);
      }
      catch (EmptyException e) {
        try {Thread.sleep(1);
        } catch(InterruptedException ignore) {;}
      }
    
  }

  //@Atomic
  public void processConfigPacket(Packet pkt) {
    Config config = pkt.config;
    int address = config.address;
    // change png val, these methods should be atomic (locked, so nothing happens in between)
    PNG.remove(address);
    PNG.add(address,config.personaNonGrata);

    // change R values
    Double low = new Double( config.addressBegin);
    Double high = new Double(config.addressEnd);
    boolean accepting = config.acceptingRange;
    if (low.equals( high) && accepting){
      low = low - .5;
      high = high +.5;
    }

    ConcurrentSkipListMap<Double,Boolean> skip = R.get(address);
    if (skip == null) {
        skip = new ConcurrentSkipListMap<Double,Boolean>(); 
        R.add(address, skip);
        //R.printTable();
    }
    
    Map.Entry<Double,Boolean> entryleft = skip.floorEntry(low);
    Map.Entry<Double,Boolean> entryright = skip.ceilingEntry(high);
    if (entryleft == null){
      skip.put(low,true);
      entryleft = skip.floorEntry(low);
      if (! accepting) skip.remove(low);
    }
    if (entryright == null ) {
      skip.put(high,false);
      entryright = skip.ceilingEntry(high);
      if (! accepting) skip.remove(high);
    }
    if (accepting) {
      // if left floor is left bound
      if (entryleft.getValue()){
        // if right ceiling is left bound CASE 1 @@@@@@@@@@@@@@@@@@@@@
        if (entryright.getValue()){
          //  System.out.println(this.id+ " CASE 1");
          // remove between entryleft and high, including high

          // System.out.println("removing in "+(entryleft.getKey()+1));
          while(skip.ceilingKey(entryleft.getKey()+1) != null && skip.ceilingKey(entryleft.getKey()+1) <= high) {
            entryleft = skip.ceilingEntry(entryleft.getKey()+1);
            skip.remove(entryleft.getKey());
          }
          // if right is greater than high, put new right bound
          if (!entryright.getKey().equals(high)) {
            skip.put(high,false);
          }
        // if right ceiling is right bound CASE 2 @@@@@@@@@@@@@@@@@@@@@@@@
        }else{
         // System.out.println(this.id+ " CASE 2");
          while( skip.ceilingKey(entryleft.getKey()+1) < entryright.getKey() ) {
            entryleft = skip.ceilingEntry(entryleft.getKey()+1);
            skip.remove(entryleft.getKey());

            //System.out.println("removing "+ entryleft.getKey());
          }
        }
      // if left floor is right bound
      }else{
        //if right celing is left bound CASE 3 @@@@@@@@@@@@@@@@@@@@@@@@@@@
        if (entryright.getValue()){
         // System.out.println(this.id +" CASE 3");
          // if neither low nor high are equal to next least/largest 
          if (!low.equals(entryleft.getKey()) && !high.equals(entryright.getKey()) ) {
            //System.out.println("neither equal"+entryright.getKey()); 
            while( skip.ceilingKey(entryleft.getKey()+1) < high) {
              entryleft = skip.ceilingEntry(entryleft.getKey() );
              skip.remove(entryleft.getKey());
              //System.out.println("removed" +entryleft.getKey());
            }
            skip.put(low,true);
            skip.put(high,false);
          // if left equal
          } else if (low.equals(entryleft.getKey()) && !high.equals(entryright.getKey()) ) {
            skip.remove(low);
            while (skip.ceilingKey(entryleft.getKey()) < high) {
              entryleft = skip.ceilingEntry(entryleft.getKey());
              skip.remove(entryleft.getKey());
            }
            skip.put(high,false);
          // if right equal
          }else if (!low.equals(entryleft.getKey()) && high.equals(entryright.getKey()) ) {
            //skip.remove(high);
            while (skip.ceilingKey(entryleft.getKey()) != null && skip.ceilingKey(entryleft.getKey()) <  high) {
              entryleft = skip.ceilingEntry(entryleft.getKey()+1);
              skip.remove(entryleft.getKey());
            }
            skip.put(low,true);
          // if both equal
          } else {
            skip.remove(low);
            while (skip.ceilingKey(entryleft.getKey()) <= high) {
              entryleft = skip.ceilingEntry(entryleft.getKey());
              skip.remove(entryleft.getKey());
            }
          }
        // if right ceiling is right bound CASE 4 @@@@@@@@@@@@@@@@@@@@@@@@@@
        }else{
          //System.out.println(this.id+" CASE 4");
          //printRange(1);
          while( skip.floorKey(entryright.getKey()-1) >= low ) {
            entryright  = skip.floorEntry(entryright.getKey()-1);
            skip.remove(entryright.getKey());
          }
          //printRange(1);
          if (!low.equals(skip.floorKey(low))) {
            skip.put(low,true);
          }
        }
      } 
    // if not acceping in this range
    }else {
      // if left ceiling is left bound
      if (entryleft.getValue()){
        // if right floor is left bound CASE 5 @@@@@@@@@@@@@@@@@@@@@@@@@@
        if (entryright.getValue()){

          //System.out.println(this.id+" CASE 5");
          //printRange(1);

          if (!low.equals( entryleft.getKey() )) {
            //System.out.println(low + " " + skip.floorKey(low));
            skip.put(low-.5, false);
          }
          if (high.equals( entryright.getKey() )) {
            skip.put(high+.5,true);
          }
          //printRange(1);
          skip.remove(low);
          skip.remove(high);
          //printRange(1);
          while (skip.ceilingKey(entryleft.getKey()+1) <= high){
            entryleft = skip.ceilingEntry(entryleft.getKey()+1);
            skip.remove(entryleft.getKey());
          }
          

       
        // if right floor is right bound CASE 6 @@@@@@@@@@@@@@@@@@@@@@
        }else{

          //System.out.println(this.id +" CASE 6 low: "+low+", "+high);
          //System.out.println("entryleft: "+entryleft.getKey()+", entryright: "+entryright.getKey());
          if (!low.equals(entryleft.getKey())) {
            skip.put(low-.5, false);
          }
          if (!high.equals(entryright.getKey())) {
            skip.put(high+.5,true);
          }
          skip.remove(low);
          skip.remove(high);
          //printRange(1);          
          //System.out.println("skip");
          while (skip.ceilingKey(entryleft.getKey()+1) != null && skip.ceilingKey(entryleft.getKey()+1) < high-.5){
            //System.out.println("hi");
            entryleft = skip.ceilingEntry(entryleft.getKey()+1);
            skip.remove(entryleft.getKey());
            if (skip.ceilingEntry(entryleft.getKey()+1) == null) {
              //System.out.println("remove rest");
              break;
            }
          }
          


        }
      // if left floor is right bound
      }else{
        //if right ceiling is left bound CASE 7 @@@@@@@@@@@@@@@@@@@@@
        if (entryright.getValue()){
          //System.out.println(this.id + " CASE 7");
          if (low.equals(entryleft.getKey())) {
            skip.put(low-.5, false);
          }
          if (high.equals(entryright.getKey()) ) {
            skip.put(high+.5,true);
          }
          skip.remove(low);
          while (skip.ceilingKey(entryleft.getKey()+1) <= high){
            entryleft = skip.ceilingEntry(entryleft.getKey()+1);
            skip.remove(entryleft.getKey());
          }
          
        //if right floor is right bound CASE 8 @@@@@@@@@@@@@@@@@@@@@
        } else {
          //System.out.println(this.id +" CASE 8");
          //printRange(1);
          //System.out.println(low + " " + skip.floorKey(low));
          if (low.equals(entryleft.getKey())) {
            //System.out.println("put: "+(low-.5) );
            skip.put(low-.5, false);
          }
          if (! high.equals( entryright.getKey() )) {
            //System.out.println("put: "+(high+.5) );
            skip.put(high+.5,true);
          }
          //printRange(1);
          skip.remove(low);
          skip.remove(high);
//          System.out.println(skip.ceilingKey(entryleft.getKey()+1)); 

          //System.out.println("hi"+skip.floorEntry(high+.5).getKey());
          while (skip.ceilingKey(entryleft.getKey()+1) != null && skip.ceilingKey(entryleft.getKey()+1) <= high-.5){
            entryleft = skip.ceilingEntry(entryleft.getKey()+1);
            skip.remove(entryleft.getKey());
          }
          //System.out.println("hi"+skip.floorEntry(high+.5).getKey());
          if (! skip.floorEntry(high).getValue() && skip.ceilingEntry(high+1)==null) {
            //System.out.println("edgecase");
            skip.remove(high+.5);
          }
        }
      }
    }
    
  }
  public void configTest() {
    Config config = new Config(1, false, true, 0, 2);
    Packet packet = new Packet(config);
    processConfigPacket(packet);
    printRange(1);

    config = new Config(1, false, true, 1,2);
    packet = new Packet(config);
    processConfigPacket(packet);
    printRange(1);

    config = new Config(1, false, true, 7,9);
    packet = new Packet(config);
    processConfigPacket(packet);
    printRange(1);
     
    config = new Config(1, false, true, 3,4);
    packet = new Packet(config);
    processConfigPacket(packet);
    printRange(1);
     
    config = new Config(1, false, false, 7,7);
    packet = new Packet(config);
    processConfigPacket(packet);
    printRange(1);
  }

  public void printRange(int address) {
    ConcurrentSkipListMap<Double,Boolean> skip = R.get(address);
    Iterator iterator = skip.entrySet().iterator();

    Map.Entry<Double,Boolean> mapEntry = skip.firstEntry();

    String parensOutput = "";
    String numbersOutput = "";
    while (mapEntry !=null) {
        parensOutput = parensOutput +" "+ ((mapEntry.getValue() == true) ? "(" : ")");
        numbersOutput = numbersOutput+" " + mapEntry.getKey() + " ";
        mapEntry = skip.ceilingEntry(mapEntry.getKey()+.5);
    }
    System.out.println(parensOutput);
    System.out.println(numbersOutput);

    

  }
}


