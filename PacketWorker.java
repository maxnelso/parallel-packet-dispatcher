import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.*;
import java.util.concurrent.ConcurrentSkipListMap;


public interface PacketWorker extends Runnable {
    public void run();
}



class SerialPacketWorker implements PacketWorker {

  PacketGenerator source;
  PaddedPrimitiveNonVolatile<Boolean> done;
  PaddedPrimitive<Boolean> memFence;
  List<AtomicInteger> histogram;
  LockFreeHashTable<Boolean> PNG;
  LockFreeHashTable<ConcurrentSkipListMap<Double,Boolean>> R;
  LockFreeHashTable<Boolean> cache;

  Fingerprint fingerprint;
  int totalPackets;

  public SerialPacketWorker(
               PacketGenerator source,
               PaddedPrimitiveNonVolatile<Boolean> done,
               PaddedPrimitive<Boolean> memFence,
               List<AtomicInteger> histogram,
               LockFreeHashTable<Boolean> PNG,
               LockFreeHashTable<ConcurrentSkipListMap<Double,Boolean>> R,
               LockFreeHashTable<Boolean> cache) {

    this.source = source;
    this.done = done;
    this.memFence = memFence;
    this.histogram = histogram;
    this.PNG = PNG;
    this.R = R;
    this.cache = cache;
    this.fingerprint = new Fingerprint();
    this.totalPackets = 0;
  }

  public void run() {

    Packet pkt;
    while (!done.value) {
      totalPackets ++;
      pkt = source.getPacket();
      switch (pkt.type) {
       case DataPacket:
         processDataPacket(pkt);
         break;
       case ConfigPacket:
         //cache.removeAll();
         processConfigPacket(pkt);
         break;
      }
    }  
  }

  public void processDataPacket( Packet pkt) {
    AddressPair sourceDestinationPair = new AddressPair(pkt.header.source,
                                                        pkt.header.dest);
    //check cache
    if (cache.contains(sourceDestinationPair.hashCode())) {
       if (cache.get(sourceDestinationPair.hashCode())) {
         long fingerprint = this.fingerprint.getFingerprint(pkt.body.iterations, pkt.body.seed);  
         int indexInHistogram = (int) fingerprint;
         histogram.get(indexInHistogram).getAndIncrement();
         //need to only remove after all the trains are done somehow, gotta have
         //a heuristic I think cause you don't actually know how many trains
         //there are
         if (pkt.header.sequenceNumber == pkt.header.trainSize) {
            cache.remove(sourceDestinationPair.hashCode());
         }
       } 
       else {
         return;
       }
    }
    Boolean png = (Boolean) PNG.get(pkt.header.source);
    if (png != null && !png) {
        ConcurrentSkipListMap skipList = (ConcurrentSkipListMap) R.get(pkt.header.source);
        Double dest = new Double(pkt.header.dest);
        Map.Entry<Double, Boolean> low = skipList.floorEntry(dest);
        //not in range, drop
        //first case, too low, no bound found
        //second case, no left bound found below me
        if (low == null || !low.getValue() ) {
            cache.add(sourceDestinationPair.hashCode(),false);
            return;
        }
        Map.Entry<Double, Boolean> high = skipList.ceilingEntry(dest);
        //not in range, drop (shouldn't have to check for null, every left
        //should be paired with a right)
        //only case is if a left bound is above me (don't even think this is
        //necessary?)
        if (high == null || high.getValue()) {
            cache.add(sourceDestinationPair.hashCode(),false);
            return;
        }
        //made it past all the checks
        long fingerprint = this.fingerprint.getFingerprint(pkt.body.iterations, pkt.body.seed);  
        int indexInHistogram = (int) fingerprint;
        histogram.get(indexInHistogram).getAndIncrement();
        cache.add(sourceDestinationPair.hashCode(),true);
    }
  }
  public void processConfigPacket(Packet pkt) {
    Config config = pkt.config;
    int address = config.address;
    // change png val, these methods should be atomic (locked, so nothing happens in between)
    PNG.remove(address);
    PNG.add(address,config.personaNonGrata);

    // change R values
    Double low = new Double( config.addressBegin);
    Double high = new Double(config.addressEnd);
    boolean accepting = config.acceptingRange;
    if (low.equals( high) && accepting){
      low = low - .5;
      high = high +.5;
    }

    ConcurrentSkipListMap<Double,Boolean> skip = R.get(address);
    if (skip == null) {
        skip = new ConcurrentSkipListMap<Double,Boolean>(); 
        R.add(address, skip);
        //R.printTable();
    }
    
    Map.Entry<Double,Boolean> entryleft = skip.floorEntry(low);
    Map.Entry<Double,Boolean> entryright = skip.ceilingEntry(high);
    if (entryleft == null){
      skip.put(low,true);
      entryleft = skip.floorEntry(low);
      if (! accepting) skip.remove(low);
    }
    if (entryright == null ) {
      skip.put(high,false);
      entryright = skip.ceilingEntry(high);
      if (! accepting) skip.remove(high);
    }
    if (accepting) {
      // if left floor is left bound
      if (entryleft.getValue()){
        // if right ceiling is left bound CASE 1 @@@@@@@@@@@@@@@@@@@@@
        if (entryright.getValue()){
        //  System.out.println(this.id+ " CASE 1");
          // remove between entryleft and high, including high

          // System.out.println("removing in "+(entryleft.getKey()+1));
          while(skip.ceilingKey(entryleft.getKey()+1) != null && skip.ceilingKey(entryleft.getKey()+1) <= high) {
            entryleft = skip.ceilingEntry(entryleft.getKey()+1);
            skip.remove(entryleft.getKey());
          }
          // if right is greater than high, put new right bound
          if (!entryright.getKey().equals(high)) {
            skip.put(high,false);
          }
        // if right ceiling is right bound CASE 2 @@@@@@@@@@@@@@@@@@@@@@@@
        }else{
         // System.out.println(this.id+ " CASE 2");
          while( skip.ceilingKey(entryleft.getKey()+1) < entryright.getKey() ) {
            entryleft = skip.ceilingEntry(entryleft.getKey()+1);
            skip.remove(entryleft.getKey());

            //System.out.println("removing "+ entryleft.getKey());
          }
        }
      // if left floor is right bound
      }else{
        //if right celing is left bound CASE 3 @@@@@@@@@@@@@@@@@@@@@@@@@@@
        if (entryright.getValue()){
         // System.out.println(this.id +" CASE 3");
          // if neither low nor high are equal to next least/largest 
          if (!low.equals(entryleft.getKey()) && !high.equals(entryright.getKey()) ) {
            //System.out.println("neither equal"+entryright.getKey()); 
            while( skip.ceilingKey(entryleft.getKey()+1) < high) {
              entryleft = skip.ceilingEntry(entryleft.getKey() );
              skip.remove(entryleft.getKey());
              //System.out.println("removed" +entryleft.getKey());
            }
            skip.put(low,true);
            skip.put(high,false);
          // if left equal
          } else if (low.equals(entryleft.getKey()) && !high.equals(entryright.getKey()) ) {
            skip.remove(low);
            while (skip.ceilingKey(entryleft.getKey()) < high) {
              entryleft = skip.ceilingEntry(entryleft.getKey());
              skip.remove(entryleft.getKey());
            }
            skip.put(high,false);
          // if right equal
          }else if (!low.equals(entryleft.getKey()) && high.equals(entryright.getKey()) ) {
            //skip.remove(high);
            while (skip.ceilingKey(entryleft.getKey()) != null && skip.ceilingKey(entryleft.getKey()) <  high) {
              entryleft = skip.ceilingEntry(entryleft.getKey()+1);
              skip.remove(entryleft.getKey());
            }
            skip.put(low,true);
          // if both equal
          } else {
            skip.remove(low);
            while (skip.ceilingKey(entryleft.getKey()) <= high) {
              entryleft = skip.ceilingEntry(entryleft.getKey());
              skip.remove(entryleft.getKey());
            }
          }
        // if right ceiling is right bound CASE 4 @@@@@@@@@@@@@@@@@@@@@@@@@@
        }else{
          //System.out.println(this.id+" CASE 4");
          //printRange(1);
          while( skip.floorKey(entryright.getKey()-1) >= low ) {
            entryright  = skip.floorEntry(entryright.getKey()-1);
            skip.remove(entryright.getKey());
          }
          //printRange(1);
          if (!low.equals(skip.floorKey(low))) {
            skip.put(low,true);
          }
        }
      } 
    // if not acceping in this range
    }else {
      // if left ceiling is left bound
      if (entryleft.getValue()){
        // if right floor is left bound CASE 5 @@@@@@@@@@@@@@@@@@@@@@@@@@
        if (entryright.getValue()){

          //System.out.println(this.id+" CASE 5");
          //printRange(1);

          if (!low.equals( entryleft.getKey() )) {
            //System.out.println(low + " " + skip.floorKey(low));
            skip.put(low-.5, false);
          }
          if (high.equals( entryright.getKey() )) {
            skip.put(high+.5,true);
          }
          //printRange(1);
          skip.remove(low);
          skip.remove(high);
          //printRange(1);
          while (skip.ceilingKey(entryleft.getKey()+1) <= high){
            entryleft = skip.ceilingEntry(entryleft.getKey()+1);
            skip.remove(entryleft.getKey());
          }
          

       
        // if right floor is right bound CASE 6 @@@@@@@@@@@@@@@@@@@@@@
        }else{

          //System.out.println(this.id +" CASE 6 low: "+low+", "+high);
          //System.out.println("entryleft: "+entryleft.getKey()+", entryright: "+entryright.getKey());
          if (!low.equals(entryleft.getKey())) {
            skip.put(low-.5, false);
          }
          if (!high.equals(entryright.getKey())) {
            skip.put(high+.5,true);
          }
          skip.remove(low);
          skip.remove(high);
          //printRange(1);          
          //System.out.println("skip");
          while (skip.ceilingKey(entryleft.getKey()+1) != null && skip.ceilingKey(entryleft.getKey()+1) < high-.5){
            //System.out.println("hi");
            entryleft = skip.ceilingEntry(entryleft.getKey()+1);
            skip.remove(entryleft.getKey());
            if (skip.ceilingEntry(entryleft.getKey()+1) == null) {
              //System.out.println("remove rest");
              break;
            }
          }
          


        }
      // if left floor is right bound
      }else{
        //if right ceiling is left bound CASE 7 @@@@@@@@@@@@@@@@@@@@@
        if (entryright.getValue()){
          //System.out.println(this.id + " CASE 7");
          if (low.equals(entryleft.getKey())) {
            skip.put(low-.5, false);
          }
          if (high.equals(entryright.getKey()) ) {
            skip.put(high+.5,true);
          }
          skip.remove(low);
          while (skip.ceilingKey(entryleft.getKey()+1) <= high){
            entryleft = skip.ceilingEntry(entryleft.getKey()+1);
            skip.remove(entryleft.getKey());
          }
          
        //if right floor is right bound CASE 8 @@@@@@@@@@@@@@@@@@@@@
        } else {
          //System.out.println(this.id +" CASE 8");
          //printRange(1);
          //System.out.println(low + " " + skip.floorKey(low));
          if (low.equals(entryleft.getKey())) {
            //System.out.println("put: "+(low-.5) );
            skip.put(low-.5, false);
          }
          if (! high.equals( entryright.getKey() )) {
            //System.out.println("put: "+(high+.5) );
            skip.put(high+.5,true);
          }
          //printRange(1);
          skip.remove(low);
          skip.remove(high);
//          System.out.println(skip.ceilingKey(entryleft.getKey()+1)); 

          //System.out.println("hi"+skip.floorEntry(high+.5).getKey());
          while (skip.ceilingKey(entryleft.getKey()+1) != null && skip.ceilingKey(entryleft.getKey()+1) <= high-.5){
            entryleft = skip.ceilingEntry(entryleft.getKey()+1);
            skip.remove(entryleft.getKey());
          }
          //System.out.println("hi"+skip.floorEntry(high+.5).getKey());
          if (! skip.floorEntry(high).getValue() && skip.ceilingEntry(high+1)==null) {
            //System.out.println("edgecase");
            skip.remove(high+.5);
          }
        }
      }
    }
  }
  public void configTest() {
    Config config = new Config(1, false, true, 0, 2);
    Packet packet = new Packet(config);
    processConfigPacket(packet);
    printRange(1);

    config = new Config(1, false, true, 1,2);
    packet = new Packet(config);
    processConfigPacket(packet);
    printRange(1);

    config = new Config(1, false, true, 7,9);
    packet = new Packet(config);
    processConfigPacket(packet);
    printRange(1);
     
    config = new Config(1, false, true, 3,4);
    packet = new Packet(config);
    processConfigPacket(packet);
    printRange(1);
     
    config = new Config(1, false, false, 7,7);
    packet = new Packet(config);
    processConfigPacket(packet);
    printRange(1);
  }

  public void printRange(int address) {
    ConcurrentSkipListMap<Double,Boolean> skip = R.get(address);
    Iterator iterator = skip.entrySet().iterator();

    Map.Entry<Double,Boolean> mapEntry = skip.firstEntry();

    String parensOutput = "";
    String numbersOutput = "";
    while (mapEntry !=null) {
        parensOutput = parensOutput +" "+ ((mapEntry.getValue() == true) ? "(" : ")");
        numbersOutput = numbersOutput+" " + mapEntry.getKey() + " ";
        mapEntry = skip.ceilingEntry(mapEntry.getKey()+.5);
    }
    System.out.println(parensOutput);
    System.out.println(numbersOutput);

    

  }
}


