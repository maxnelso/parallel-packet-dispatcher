import java.util.HashMap;
import java.util.concurrent.locks.*;
import java.util.*;
import java.util.concurrent.atomic.*;
import java.util.concurrent.ConcurrentSkipListMap;
class SerialFirewallTest {
  public static void main(String[] args) {

    if (args.length < 11) {
        System.out.println("Usage: numMilliseconds numAddressesLog numTrainsLog meanTrainSize meanTrainsPerComm meanWindow meanCommsPerAddress meanWork configFraction pngFraction acceptingFraction");
        System.exit(1);
    }
    final int numMilliseconds = Integer.parseInt(args[0]);    
    final int numAddressesLog = Integer.parseInt(args[1]);    
    final int numTrainsLog = Integer.parseInt(args[2]);    
    final double meanTrainSize = Double.parseDouble(args[3]);    
    final double meanTrainsPerComm = Double.parseDouble(args[4]);    
    final int meanWindow = Integer.parseInt(args[5]);    
    final int meanCommsPerAddress = Integer.parseInt(args[6]);    
    final int meanWork = Integer.parseInt(args[7]);    
    final double configFraction = Double.parseDouble(args[8]);
    final double pngFraction = Double.parseDouble(args[9]);
    final double acceptingFraction = Double.parseDouble(args[10]);
    final int maxBucketSize = 16;
    final int numThreads = 1;
    
    /*
    System.out.println("Number of milliseconds: " + numMilliseconds);
    System.out.println("Number of Addresses (Log): " + numAddressesLog);
    System.out.println("Number of Trains (Log): " + numTrainsLog);
    System.out.println("Mean Train Size: " + meanTrainSize);
    System.out.println("Mean Trains Per Communication: " + meanTrainsPerComm);
    System.out.println("Mean Window Size: " + meanWindow);
    System.out.println("Mean Communications Per Address: " + meanCommsPerAddress);
    System.out.println("Mean Work: " + meanWork);
    System.out.println("Configuration Packet Fraction: " + configFraction);
    System.out.println("PNG Packet Fraction: " + pngFraction);
    System.out.println("Accepting Packet Fraction: " + acceptingFraction);
    */
    @SuppressWarnings({"unchecked"})
    StopWatch timer = new StopWatch();
    PacketGenerator source = new PacketGenerator(numAddressesLog,
                                                 numTrainsLog,
                                                 meanTrainSize,
                                                 meanTrainsPerComm,
                                                 meanWindow,
                                                 meanCommsPerAddress,
                                                 meanWork,
                                                 configFraction,
                                                 pngFraction,
                                                 acceptingFraction);

    PaddedPrimitiveNonVolatile<Boolean> done = new PaddedPrimitiveNonVolatile<Boolean>(false);
    PaddedPrimitive<Boolean> memFence = new PaddedPrimitive<Boolean>(false);
    //kind of annoying to do for serial :P
    
    timer.startTimer();
    // create dictionary (histogram) of fingerprints processed, worker thread increments dictionary
//    LockFreeHashTable histogram = new LockFreeHashTable(1,numThreads,maxBucketSize); 
   // int[] histogram = new int[65536];
    List<AtomicInteger> histogram = new ArrayList<AtomicInteger>(65536);
    // create dictionary PNG of boolean values for each S
    LockFreeHashTable<Boolean> PNG = new LockFreeHashTable(1,numThreads,maxBucketSize);
    // create dictionary R of skip lists for the set of addresses S accepted by each D
    LockFreeHashTable<ConcurrentSkipListMap<Double,Boolean>> R = new LockFreeHashTable(1,numThreads,maxBucketSize);
    // create dictionary of (S,D) pairs currently in use (cache)
    LockFreeHashTable<Boolean> cache = new LockFreeHashTable(1,numThreads,maxBucketSize);


    SerialPacketWorker worker = new SerialPacketWorker(source, 
                                                       done, 
                                                       memFence,
                                                       histogram,
                                                       PNG,
                                                       R,
                                                       cache); // + any data structures
    Thread workerThread = new Thread(worker);
    for (int i = 0 ; i < Math.pow(Math.pow(2,numAddressesLog),1.5);i++) {
        Packet packet = source.getConfigPacket();
        worker.processConfigPacket(packet);
    }
    workerThread.start();
    try {
      Thread.sleep(numMilliseconds);
    } catch (InterruptedException ignore) {;}
    done.value = true;
    memFence.value = true;
    try {
        workerThread.join();
    } catch (InterruptedException ignore) {;}
    timer.stopTimer();
    int totalCount = worker.totalPackets;
    /*
    System.out.println("count: " + totalCount);
    System.out.println("time: " + timer.getElapsedTime());
    */
    String label = "";
    for (int i = 0 ; i < 11;i++) {
        label = label + args[i] + " ";
    }
    System.out.println("Serial " + label);
    System.out.println(new Double(totalCount)/timer.getElapsedTime() + " pkts / ms");
  }
}


class ParallelFirewallTest {
  public static void main(String[] args) {

    if (args.length < 12) {
        System.out.println("Usage: numMilliseconds numAddressesLog numTrainsLog meanTrainSize meanTrainsPerComm meanWindow meanCommsPerAddress meanWork configFraction pngFraction acceptingFraction numWorkers");
        System.exit(1);
    }
    final int numMilliseconds = Integer.parseInt(args[0]);    
    final int numAddressesLog = Integer.parseInt(args[1]);    
    final int numTrainsLog = Integer.parseInt(args[2]);    
    final double meanTrainSize = Double.parseDouble(args[3]);    
    final double meanTrainsPerComm = Double.parseDouble(args[4]);    
    final int meanWindow = Integer.parseInt(args[5]);    
    final int meanCommsPerAddress = Integer.parseInt(args[6]);    
    final int meanWork = Integer.parseInt(args[7]);    
    final double configFraction = Double.parseDouble(args[8]);
    final double pngFraction = Double.parseDouble(args[9]);
    final double acceptingFraction = Double.parseDouble(args[10]);
    final int numWorkers = Integer.parseInt(args[11]);
    
    final int maxBucketSize = 16;
    /*
    System.out.println("Number of milliseconds: " + numMilliseconds);
    System.out.println("Number of Addresses (Log): " + numAddressesLog);
    System.out.println("Number of Trains (Log): " + numTrainsLog);
    System.out.println("Mean Train Size: " + meanTrainSize);
    System.out.println("Mean Trains Per Communication: " + meanTrainsPerComm);
    System.out.println("Mean Window Size: " + meanWindow);
    System.out.println("Mean Communications Per Address: " + meanCommsPerAddress);
    System.out.println("Mean Work: " + meanWork);
    System.out.println("Configuration Packet Fraction: " + configFraction);
    System.out.println("PNG Packet Fraction: " + pngFraction);
    System.out.println("Accepting Packet Fraction: " + acceptingFraction);
    System.out.println("Number of workers: "  + numWorkers);
    */
    @SuppressWarnings({"unchecked"})
    StopWatch timer = new StopWatch();
    PacketGenerator source = new PacketGenerator(numAddressesLog,
                                                 numTrainsLog,
                                                 meanTrainSize,
                                                 meanTrainsPerComm,
                                                 meanWindow,
                                                 meanCommsPerAddress,
                                                 meanWork,
                                                 configFraction,
                                                 pngFraction,
                                                 acceptingFraction);

    PaddedPrimitiveNonVolatile<Boolean> done = new PaddedPrimitiveNonVolatile<Boolean>(false);
    PaddedPrimitive<Boolean> memFence = new PaddedPrimitive<Boolean>(false);

    List<AtomicInteger> histogram = new ArrayList<AtomicInteger>(65536);
    // create dictionary PNG of boolean values for each S
    LockFreeHashTable<Boolean> PNG = new LockFreeHashTable(256,numWorkers,maxBucketSize);
    // create dictionary R of skip lists for the set of addresses S accepted by each D
    LockFreeHashTable<ConcurrentSkipListMap<Double,Boolean>> R = new LockFreeHashTable(256,numWorkers,maxBucketSize);
    // create dictionary of (S,D) pairs currently in use (cache)
    LockFreeHashTable<Boolean> cache = new LockFreeHashTable(1,numWorkers,maxBucketSize);
    

    //initialize queues/workers, change the portion for the fraction later
    LamportQueue[] configQueues = new LamportQueue[numWorkers/3+1];
    LamportQueue[] dataQueues = new LamportQueue[(2*numWorkers)/3];

    ConfigWorker[] configWorkers = new ConfigWorker[numWorkers/3+1];
    DataWorker[] dataWorkers = new DataWorker[(2*numWorkers)/3];

    Thread[] configThreads = new Thread[numWorkers/3 + 1];
    Thread[] dataThreads = new Thread[(2*numWorkers)/3];
    for (int i = 0; i < numWorkers/3 + 1; i++) {
        ReentrantLock lock = new ReentrantLock();
        //enforce constraint of 256 packets out in the wild at a given time
        configQueues[i] = new LamportQueue(255/numWorkers,lock);
        ConfigWorker configWorker = new ConfigWorker(source,
                                                     done,
                                                     memFence,
                                                     histogram,
                                                     PNG,
                                                     R,
                                                     cache,
                                                     configQueues,
                                                     i);
        configWorkers[i] = configWorker;
        configThreads[i] = new Thread(configWorker);

    } 
    for (int i = 0; i < (2*numWorkers)/3; i++) {
        ReentrantLock lock = new ReentrantLock();
        //enforce constraint of 256 packets out in the wild at a given time
        dataQueues[i] = new LamportQueue(255/numWorkers,lock);
        DataWorker dataWorker = new DataWorker(source,
                                               done,
                                               memFence,
                                               histogram,
                                               PNG,
                                               R,
                                               cache,
                                               dataQueues,
                                               i);
        dataWorkers[i] = dataWorker;
        dataThreads[i] = new Thread(dataWorker);
    } 

    Dispatcher dispatcher = new Dispatcher(configQueues, dataQueues, source,numWorkers,done);
    Thread dispatcherThread = new Thread(dispatcher);
    
    //initialize
    for (int i = 0 ; i < Math.pow(Math.pow(2,numAddressesLog),1.5);i++) {
        Packet packet = source.getConfigPacket();
        configWorkers[0].processConfigPacket(packet);
    }
    
     
    timer.startTimer();
    //start dispatcher
    dispatcherThread.start();
    for (int i = 0; i < (2*numWorkers)/3;i++) {
        if (i < numWorkers/3 + 1) {
            configThreads[i].start();
            dataThreads[i].start();
        }
        else {
            dataThreads[i].start();
        }
    }
    //start all config and data threads
    try {
      Thread.sleep(numMilliseconds);
    } catch (InterruptedException ignore) {;}
    done.value = true;
    memFence.value = true;
    try {
        dispatcherThread.join();
        for (int i = 0; i < (2*numWorkers)/3;i++) {
            if (i < numWorkers/3 + 1) {
                configThreads[i].join();
                dataThreads[i].join();

            }
            else {
                dataThreads[i].join();

            }
        }
        //join dispatcher, and all config and data threads
    } catch (InterruptedException ignore) {;}      
    timer.stopTimer();
    int totalCount = 0;
    for (int i = 0; i < (2*numWorkers)/3;i++) {
        if (i < numWorkers/3 + 1) {
            totalCount = totalCount + configWorkers[i].totalPackets;
            totalCount = totalCount + dataWorkers[i].totalPackets;
        }
        else {
            totalCount = totalCount + dataWorkers[i].totalPackets;
        }
    }
    /*
    System.out.println("count: " + totalCount);
    System.out.println("time: " + timer.getElapsedTime());
    */
    String label = "";
    for (int i = 0 ; i < 12;i++) {
        label = label + args[i] + " ";
    }
    System.out.println("Parallel " + label);
    System.out.println(new Double(totalCount)/timer.getElapsedTime() + " pkts / ms");
  }
}

