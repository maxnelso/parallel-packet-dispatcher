class EmptyException extends Exception {
      public EmptyException() {
            super("The queue is full!");
      }
             
}
