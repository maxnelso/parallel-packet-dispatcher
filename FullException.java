class FullException extends Exception {
    public Packet packet;
      public FullException(Packet packet) {
          super("The queue is full!");
          this.packet = packet;
      }
             
}
