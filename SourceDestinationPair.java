public class SourceDestinationPair {

    int source;
    int destination;

    public SourceDestinationPair(int source, int destination) {
        this.source = source;
        this.destination = destination;
    }


    public boolean equals(Object other){
            boolean result;
            if((other == null) || (getClass() != other.getClass())){
                result = false;
            } 
            else{
                SourceDestinationPair otherPair = (SourceDestinationPair) other;
                result = this.source == otherPair.source && this.destination == otherPair.destination;
            } 
            return result;
    } 

}
