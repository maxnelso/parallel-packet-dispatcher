import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.*;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.Random;
import org.deuce.Atomic;


class DataWorker implements PacketWorker {

  PacketGenerator source;
  PaddedPrimitiveNonVolatile<Boolean> done;
  PaddedPrimitive<Boolean> memFence;
  List<AtomicInteger> histogram;
  LockFreeHashTable<Boolean> PNG;
  LockFreeHashTable<ConcurrentSkipListMap<Double,Boolean>> R;
  LockFreeHashTable<Boolean> cache;
  LamportQueue[] dataQueues;
  int id;

  Fingerprint fingerprint;
  int totalPackets;

  public DataWorker(
               PacketGenerator source,
               PaddedPrimitiveNonVolatile<Boolean> done,
               PaddedPrimitive<Boolean> memFence,
               List<AtomicInteger> histogram,
               LockFreeHashTable<Boolean> PNG,
               LockFreeHashTable<ConcurrentSkipListMap<Double,Boolean>> R,
               LockFreeHashTable<Boolean> cache,
               LamportQueue[] dataQueues,
               int id) {

    this.source = source;
    this.done = done;
    this.memFence = memFence;
    this.histogram = histogram;
    this.PNG = PNG;
    this.R = R;
    this.cache = cache;
    this.dataQueues = dataQueues;
    this.id = id;
    this.fingerprint = new Fingerprint();
    this.totalPackets = 0;
  }

  public void run() {
    Random random = new Random();
    int queueIndex = -1;
    while (!done.value) {
      //hand craft this later
      //queueIndex = random.nextInt(this.dataQueues.length);
      processPacket(this.id);
    }  
    //flush out those in my queue
    while (!this.dataQueues[this.id].isEmpty()) {
        processPacket(this.id);
    }
  }
  @Atomic
  public void processPacket(int queueIndex) {
      try {
        Packet packet = this.dataQueues[queueIndex].dequeue();
        processDataPacket(packet);
        totalPackets++;
      }
      catch (EmptyException e) {
        try {Thread.sleep(1);
        }catch(InterruptedException ignore){;}
      }
    
  }

  //@Atomic
  public void processDataPacket( Packet pkt) {
    AddressPair sourceDestinationPair = new AddressPair(pkt.header.source,
                                                        pkt.header.dest);
    //check cache
    /*if (cache.contains(sourceDestinationPair.hashCode())) {
       if (cache.get(sourceDestinationPair.hashCode())) {
         long fingerprint = this.fingerprint.getFingerprint(pkt.body.iterations, pkt.body.seed);  
         int indexInHistogram = (int) fingerprint;
         histogram.get(indexInHistogram).getAndIncrement();
         //need to only remove after all the trains are done somehow, gotta have
         //a heuristic I think cause you don't actually know how many trains
         //there are
         if (pkt.header.sequenceNumber == pkt.header.trainSize) {
            cache.remove(sourceDestinationPair.hashCode());
         }
       } 
       else {
         return;
       }
    }*/
    if (pkt.header == null) {
      System.out.println("bad");
    }
    Boolean png = (Boolean) PNG.get(pkt.header.source);
    if (png != null && !png) {
        ConcurrentSkipListMap skipList = (ConcurrentSkipListMap) R.get(pkt.header.source);
        Double dest = new Double(pkt.header.dest);
        if (skipList == null) {
          return;
        }
        Map.Entry<Double, Boolean> low = skipList.floorEntry(dest);
        //not in range, drop
        //first case, too low, no bound found
        //second case, no left bound found below me
        if (low == null || !low.getValue() ) {
      //      cache.add(sourceDestinationPair.hashCode(),false);
            return;
        }
        Map.Entry<Double, Boolean> high = skipList.ceilingEntry(dest);
        //not in range, drop (shouldn't have to check for null, every left
        //should be paired with a right)
        //only case is if a left bound is above me (don't even think this is
        //necessary?)
        if (high == null || high.getValue()) {
       //     cache.add(sourceDestinationPair.hashCode(),false);
            return;
        }
        //made it past all the checks
        long fingerprint = this.fingerprint.getFingerprint(pkt.body.iterations, pkt.body.seed);  
        int indexInHistogram = (int) fingerprint;
        histogram.get(indexInHistogram).getAndIncrement();
        //cache.add(sourceDestinationPair.hashCode(),true);
    }
  }
}
